/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 15:51:23 by apakhomo          #+#    #+#             */
/*   Updated: 2018/01/30 15:51:23 by apakhomo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERROR_H
# define ERROR_H
# define ER00 "[!] Error: no ants.\n"
# define ER01 "[!] Error: STDIN don't read.\n"
# define ER02 "[!] Error: invalid ants.\n"
# define ER03 "[!] Error: too many ants.\n"
# define ER04 "[!] Error: incorrect start/end\n"
# define ER05 "[!] Error: incorrect room name\n"
# define ER06 "[!] Error: RLY? 1000+ rooms?\n"
# define ER07 "[!] Error: invalid room coordinates\n"
# define ER08 "[!] Error: invalid room name in links\n"
# define ER09 "[!] Error: invalid linking room\n"

#endif
